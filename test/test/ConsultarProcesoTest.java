package test;

import com.absoft.core.VigileApp;
import com.absoft.model.Message;
import com.absoft.model.Process;
import com.absoft.model.ProcessData;
import com.absoft.services.db.express.ProcessService;

public class ConsultarProcesoTest {
	public static void main(String[] args) {

		VigileApp.init();
		
		String codProceso = "11001311002620170050400";
		String codCiudad = "11001";
		String codEntidad = "347-True-3110-11001-Juzgado de Circuito-Familia";
		ProcessData datos = new ProcessData();
		datos.setCiudad(codCiudad);
		datos.setEntidad(codEntidad);
		datos.setCodProceso(codProceso);
		Process entrada = new Process();
		entrada.setData(datos);
		ProcessService servicio = new ProcessService();
		
		Message<String> salida = servicio.validarProceso(entrada);

		System.out.println(salida.getMsg());
		System.exit(0);
	}
}

//{
//	  "codProceso": "11001311002820190005400",
//	  "ciudad": "11001",
//	  "entidad": "347-True-3110-11001-Juzgado de Circuito-Familia"
//	}